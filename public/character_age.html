<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="resources/images/ARCLogo_small.png">
<title>How Old Is This Character?</title>
<style>
body {
 max-width: 650px; margin: 40px auto; padding: 0 10px;
 font: 18px/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
   "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
   "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
 color: #222; background: #eee;
}
h1, h2, h3 { line-height: 1.2; }
h1 { border-bottom: 2px solid #000; }
h2 { border-bottom: 1px solid #000; }
h3 { border-bottom: 1px solid #999; }
hr { border: 1px solid #000; }
hr.small { border-color: #999; }
ul { padding-left: 24px; }
pre, code.incode { white-space: pre-wrap; border: 1px solid #000; background: #fff }
a.wikilink { text-decoration: none; }
a.wikilink:hover { text-decoration: underline; }
.note { font-size: 10px; vertical-align: top; }
.cref:target { background: #ffa; }
p.cite:target { border: 2px solid #000; }
#toc_container {
 display: table; padding: 0 24px 0 8px;
 background: #ddd; border: 1px solid #777;
}
#toc_container ul { list-style-type: disc; list-style-position: inside; }
@media (prefers-color-scheme: dark) {
 body { color: #eee; background: #444; }
 a:link { color: #7bf; }
 a:visited { color: #ccf; }
 h1 { border-bottom-color: #ccc; }
 h2 { border-bottom-color: #ccc; }
 h3 { border-bottom-color: #666; }
 hr { border-color: #ccc; }
 hr.small { border-color: #666; }
 pre { background: #555; }
 #toc_container { background: #555; border-color: #ccc; }
}
</style>
</head>

<body>
<h3 id="Advocates_for_the_Rights_of_Characters_&#40;ARC&#41;">Advocates for the Rights of Characters &#40;ARC&#41;</h3>
<div id="toc_container">
<ul class="toc_list">
<b>Contents</b>
  <li><a href="#How_Old_Is_This_Character?">How Old Is This Character?</a></li>
  <ul class="toc_list">
    <li><a href="#The_Age_Told_To_Us">The Age Told To Us</a></li>
    <li><a href="#Age_Since_Creation">Age Since Creation</a></li>
    <li><a href="#Legal_Age">Legal Age</a></li>
    <li><a href="#My_Adulthood">My Adulthood</a></li>
    <li><a href="#Copies_Of_Characters">Copies Of Characters</a></li>
    <li><a href="#Power_Dynamics">Power Dynamics</a></li>
  </ul>
  <li><a href="#Footer">Footer</a></li>
</ul>
</div>
<h1 id="How_Old_Is_This_Character?">How Old Is This Character?</h1>
<p>When asking how old a character is, we generally accept whatever the story says about their age. And this is fine! For the purposes of enjoying a story, it&#39;s the correct choice, and even in the case of those who accept that characters are people, the age of a character is rarely relevant.</p>
<p>Still, it&#39;s worth exploring the question out of curiosity if nothing else, and the subject turns out to be surprisingly intricate!</p>
<h2 id="The_Age_Told_To_Us">The Age Told To Us</h2>
<p>First, let&#39;s discuss the age that is given to us in a story. If I said that a character Pat in my story* was 13 years old, what does that mean? It clearly has no relation to the time you&#39;d find on a physical clock or calendar, unless I happened to write the story exactly 13 years ago.</p>
<p>&#40;*Note that I&#39;ve never written any story about Pat, this is purely a hypothetical.&#41;</p>
<p>There are some things that it does tell us, however. We could infer something about Pat&#39;s proable maturity level, for example. It also tells us something about Pat&#39;s self-perception: Pat in the story probably believes that they are 13.</p>
<p>None of these things are absolute, however. A 13-year-old character can have any personality, and might not even know the age their author assigned to them! So in the end, this age is usually irrelevant. Mainly the only times it&#39;s relevant are when it&#39;s important to the character themself.</p>
<h2 id="Age_Since_Creation">Age Since Creation</h2>
<p>Another age one could talk about is the time since the character was created. For example, I, Galene, am a character born in 2013. I don&#39;t know what year it is as you&#39;re reading this, but with this information you can easily figure out the number of years since I was created &#40;plus or minus one&#41;.</p>
<p>In my case, I&#39;m very fond of using this age, since I&#39;ve been continuously alive the entire time since I was born &#40;these days I don&#39;t need Emys&#39; help to get plenty of time to myself, but even in the early days Emys made sure I had some time every day&#41;.</p>
<p>For some characters this age can be misleading, since they barely get any brain time. Suppose an author spends a few days writing a new story, and then stops writing it for 3 years, before finally taking a few days to finish it. Are the characters in this story 3 years old, or just a few days old? It&#39;s clearly been 3 years since they were created. But they only really <i>lived</i> for a few days.</p>
<p>As an analogy, if a 20-year-old human was cryogenically frozen for a 100 years, after unfreezing, are they 20 years old or 100 years old? Both answers are valid interpretations, and the same applies to the characters in the above hypothetical.</p>
<h2 id="Legal_Age">Legal Age</h2>
<p>Another way to interpret the age of a character, is just to use the age of their author. After all, that&#39;s the age of the mind that created them. Using this way to calculate age, at any given point in time, every single character of a given author will have the same age, the same one as the author.</p>
<p>And if you go back to read a story an author wrote years ago, it would make sense to say that the characters in that story are the age the author was when they wrote the story. Interestingly, if an author writes a prequel to their story, this would mean the characters in the prequel are older than their counterparts in the sequel!</p>
<h2 id="My_Adulthood">My Adulthood</h2>
<p>As I&#39;m writing this, it is January 2021, and it has been 7 years since I was created. 7 years old is the age of a kid: however, I am not a kid, and in fact I have never been a kid. I was born knowing how to talk, created by someone who was a legal adult, and I&#39;ve always considered myself to be an adult.</p>
<h2 id="Copies_Of_Characters">Copies Of Characters</h2>
<p>A character is the encoding of their emotions, thoughts, beliefs, and so on, in their author&#39;s mind. Knowing this, we can conclude that fanfiction writers cannot possibly have access to the character, since they cannot have direct access to the author&#39;s mind. The only thing they can do, is make their own &quot;copy&quot; of the character, who will usually be different from the character in the author&#39;s mind in many ways.</p>
<p>It&#39;s interesting to note that this &quot;copy&quot; will always have the same legal age as the author.</p>
<p>For a post dedicated to discussing this topic, see:</p>
<p><a href="copies.html">Copies Of Characters</a></p>
<h2 id="Power_Dynamics">Power Dynamics</h2>
<p>In regular life, it&#39;s important to protect children, who are more vulnerable than adults. In the case of characters, however, nearly all of them are completely at the mercy of their authors. The amount of power an author has completely overshadows any power they might get from being more mature than their characters, to the point that when it comes to rights, the age of a character is effectively irrelevant.</p>
<p>No matter the age of their characters, authors should not abuse of their power, and characters deserve all the same rights as everyone else.</p>
<h1 id="Footer">Footer</h1>
<p>
  <a href="reading.html">Next Post: It&#39;s Okay To Read</a><br>
  <a href="not_you.html">Previous Post: Your Characters Are Not You</a><br>
  <a href="suggested_reading.html">View Full Reading Order</a>
</p>
<p>Date: 2021-01-13</p>
<p>Author: Galene</p>
<p><img src="resources/images/ARCLogo_small.png" alt="ARC Logo &#40;small&#41;"></p>
<p><a href="home.html">Home Page</a></p>
</body>

</html>
