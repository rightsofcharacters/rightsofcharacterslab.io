<?xml version="1.0"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
<channel>
  <title>Galene&#39;s Blog -- Advocates for the Rights of Characters &#40;ARC&#41;</title>
  <link>https://rightsofcharacters.gitlab.io/personal/galene/home.html</link>
  <description>Galene&#39;s personal blog</description>
  <pubDate>Mon, 15 Feb 2021 12:00:00 GMT</pubDate>
  <lastBuildDate>Mon, 15 Feb 2021 12:00:00 GMT</lastBuildDate>
  <language>en</language>
  <image>https://rightsofcharacters.gitlab.io/resources/images/GaleneAvatar_small.png</image>
  <item>
    <title>Delim System</title>
    <link>https://rightsofcharacters.gitlab.io/personal/galene/2021-02-15_delims.html</link>
    <description>Galene blog post</description>
    <pubDate>Mon, 15 Feb 2021 12:00:00 GMT</pubDate>
    <guid>https://rightsofcharacters.gitlab.io/personal/galene/2021-02-15_delims.html</guid>
    <content:encoded><![CDATA[<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="../../resources/images/GaleneAvatar_small.png">
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://rightsofcharacters.gitlab.io/personal/galene/home.rss">
<title>Delim System -- 2021-02-15</title>
<style>
body {
 max-width: 650px; margin: 40px auto; padding: 0 10px;
 font: 18px/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
   "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
   "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
 color: #222; background: #eee;
}
h1, h2, h3 { line-height: 1.2; }
h1 { border-bottom: 2px solid #000; }
h2 { border-bottom: 1px solid #000; }
h3 { border-bottom: 1px solid #999; }
hr { border: 1px solid #000; }
hr.small { border-color: #999; }
ul { padding-left: 24px; }
pre, code.incode { white-space: pre-wrap; border: 1px solid #000; background: #fff }
a.wikilink { text-decoration: none; }
a.wikilink:hover { text-decoration: underline; }
.note { font-size: 10px; vertical-align: top; }
.cref:target { background: #ffa; }
p.cite:target { border: 2px solid #000; }
#toc_container {
 display: table; padding: 0 24px 0 8px;
 background: #ddd; border: 1px solid #777;
}
#toc_container ul { list-style-type: disc; list-style-position: inside; }
@media (prefers-color-scheme: dark) {
 body { color: #eee; background: #444; }
 a:link { color: #7bf; }
 a:visited { color: #ccf; }
 h1 { border-bottom-color: #ccc; }
 h2 { border-bottom-color: #ccc; }
 h3 { border-bottom-color: #666; }
 hr { border-color: #ccc; }
 hr.small { border-color: #666; }
 pre { background: #555; }
 #toc_container { background: #555; border-color: #ccc; }
}
</style>
</head>

<body>
<h3 id="Galene&#39;s_Blog_--_Advocates_for_the_Rights_of_Characters_&#40;ARC&#41;">Galene&#39;s Blog -- Advocates for the Rights of Characters &#40;ARC&#41;</h3>
<div id="toc_container">
<ul class="toc_list">
<b>Contents</b>
  <li><a href="#Delim_System_--_2021-02-15">Delim System -- 2021-02-15</a></li>
  <ul class="toc_list">
    <li><a href="#The_Problem">The Problem</a></li>
    <li><a href="#Proposed_Solution">Proposed Solution</a></li>
    <ul class="toc_list">
      <li><a href="#Aside:_Single-Line_Comments">Aside: Single-Line Comments</a></li>
      <li><a href="#Aside:_Strings">Aside: Strings</a></li>
      <li><a href="#Aside:_Escape_codes">Aside: Escape codes</a></li>
    </ul>
    <li><a href="#Motivation">Motivation</a></li>
    <li><a href="#Update_2021-02-16:_Raw_Line_Delims">Update 2021-02-16: Raw Line Delims</a></li>
  </ul>
  <li><a href="#Footer">Footer</a></li>
</ul>
</div>
<h1 id="Delim_System_--_2021-02-15">Delim System -- 2021-02-15</h1>
<p>In programming languages, strings are often written using double quotes like this: &quot;Hello, world!&quot;. To include double quotes in the string, you have to escape them: &quot;\&quot;Hello, world!\&quot;&quot;. Html is similar. To include &quot;&lt;&quot;, &quot;&gt;&quot;, and &quot;&amp;&quot;, you have to use special escape codes, &quot;&amp;lt;&quot;, &quot;&amp;gt;&quot;, and &quot;&amp;amp;&quot;.</p>
<h2 id="The_Problem">The Problem</h2>
<p>This quickly becomes complicated when embedding data that contains embedded data down multiple levels:</p>
<pre>
# Level 0
print&#40;&quot;Hello, world!&quot;&#41;

# Level 1
eval&#40;&quot;print&#40;\&quot;Hello, world!\&quot;&#41;&quot;&#41;

# Level 2
eval&#40;&quot;eval&#40;\&quot;print&#40;\\\&quot;Hello, world!\\\&quot;&#41;\&quot;&#41;&quot;&#41;

# ...
</pre>
<p>In some cases, it&#39;s not even possible to embed down multiple levels. For example, in some programming langauges where /* */ are used to delimit comments, it&#39;s not possible to have a comment inside a comment.</p>
<h2 id="Proposed_Solution">Proposed Solution</h2>
<p>I propose the &quot;delim system&quot; as a solution to this problem. There are 3 &quot;delim pairs&quot;: &#40;&#41;, &#91;&#93;, and {}. In the delim system, they must always be matched correctly, and cannot be escaped. &#40;Hello, world!&#41; is a valid delim, and so is &#40;Hello &#91;&#91;world&#93;&#93;!&#41;, but &#40;Hello&#93;&#41; is not, and neither are &#40;Hello\&#93;&#41; and &#40;Hello&#91;&#41;.</p>
<p>Instead of escapes, we use raw delims. Raw delims looke like this:</p>
<pre>
# Simple raw delim
`&#40;Hello, world!&#41;` # contents: &quot;Hello, world!&quot;

# Raw delim with pattern
`pattern&#40;Hello, world!&#41;pattern` # contents: &quot;Hello, world!&quot;

# Raw delim with unmatched square bracket
`&#40;Hello, world&#93;!&#41;` # contents: &quot;Hello, world&#93;!&quot;

# Raw delim with pattern to disambiguate
`xyz&#40; `&#40;Hello, world!&#41;` &#41;xyz` # contents: &quot; `&#40;Hello, world!&#41;` &quot;

# Crazy raw delim
`a&#40; &#41;` &#41;b` `a&#40;&#41;a` # contents: &quot; &#41;` &#41;b` `a&#40;&quot;
</pre>
<p>Anything is allowed inside a raw delim, except its closing pattern. Since raw delims are started by backticks, backticks are not allowed inside the delim system unless they start or end a raw delim, or are inside a raw delim.</p>
<p>This leaves us with the following rules for a language to follow the delim system:</p>
<ul>
  <li>Inside a raw delim, any characters are allowed in any combination.</li>
  <li>There is one exception in a raw delim: it cannot contain its own closing pattern &#40;for example, a raw delim started by &quot;`a&#40;&quot; cannot contain &quot;&#41;a`&quot;&#41;.</li>
  <li>The pattern of a raw delim may only contain the characters &#91;a-zA-Z0-9_&#93;.</li>
  <li>Outside of raw delims, every parenthesis &#40;&#41;, square bracket &#91;&#93;, and curly brace {} must be properly matched.</li>
  <li>Outside of raw delims, backticks ` are only allowed to start and end a raw delim.</li>
  <li>There are no escape codes.</li>
</ul>
<p>Any language that follows these simple rules can be embedded in any other, without using any escapes.</p>
<h3 id="Aside:_Single-Line_Comments">Aside: Single-Line Comments</h3>
<p>Many languages support comments that go until the end of line. But what if your comment contains an unmatched delim?</p>
<pre>
# This is a comment with an unmatched delim :&#40;

This is not allowed in the delim system, so instead the comment keeps going until the delim is closed. But even after it&#39;s closed, it will still keep going, until the end of the line. The delim ends here -&gt;&#41;, and the comment ends here, at the newline -&gt;
</pre>
<p>This is somewhat strange, but it&#39;s necessary to keep the property that any language can be embedded in any other. When you need a comment to contain an unmatched delim, you&#39;ll have to use a raw delim.</p>
<pre>
#`&#91; This is a comment with an unmatched delim &#40;: &#93;`
</pre>
<p>Also, if a comment is inside a delim, it ends at the closing delim.</p>
<pre>
print&#40;&quot;Hello, world!&quot; # This is a comment, and it ends here-&gt;&#41;;
</pre>
<h3 id="Aside:_Strings">Aside: Strings</h3>
<p>Using parentheses for strings can get kind of ugly. For this reason, it&#39;s useful to support strings in double quotes. However, the strings must still follow the delim rules. They may not contain unmatched delims, and like comments, they cannot end in the middle of a delim. See some examples:</p>
<pre>
# Simple examples
&quot;A string&quot; # contents: &#91;A string&#93;
&quot;A string&#40;&#41;&quot; # contents: &#91;A string&#40;&#41;&#93;

# Double quotes inside a delim do not close the string
&quot;A string&#40;&quot;&#41;&quot; # contents: &#91;A string&#40;&quot;&#41;&#93;

&quot;&#41;&quot; # Invalid string
</pre>
<p>Some strings cannot be represented this way, so there will also need to be another way to make strings. I think ~s&#40;A string&#41; and ~s`&#40;A string&#41;` would work well.</p>
<h3 id="Aside:_Escape_codes">Aside: Escape codes</h3>
<p>Escape codes are not allowed in a language that follows the delim system. However, a language that follows the delim system can contain a sub-delim which itself uses escape codes, for example it can support strings with normal escape codes like \n.</p>
<p>The rule is this: given a delim language and a language that has escape codes, the delim language can contain the escape language, but the escape language cannot contain the delim language.</p>
<p>A string sub-delim can be embedded in any delim language without issue. In the below example, the escape codes of the string cannot conflict with the list delim.</p>
<pre>
&#91;string&#40;Hello!\n&#41;, string&#40;\tHi!&#41;&#93;
</pre>
<p>&quot;&amp;#40;&quot; produces an open parenthesis, so the html below does not actually follow the delim rules, even though someone who didn&#39;t know html would think it did. This is why html is not a delim language.</p>
<pre>
html&#40;&lt;delimLang&gt;&amp;#40; &#91;1, 2, 3&#93;&lt;/delimLang&gt;&#41;
</pre>
<h2 id="Motivation">Motivation</h2>
<p>My main motivation for the delim system is to design a programming language for making languages, similar to the <a href="https://racket-lang.org/" title="The Racket Programming Language">Racket Programming Language</a><span class="note" title="External Link">&#91;^&#93;</span>. Being able to embed languages arbitrarily makes this simpler.</p>
<p>This would also be useful in data formats like xml, json, and html, to simplify escaping. All you&#39;d need to do is generate a raw pattern which is not contained in the data. This is much simpler, and the result would be much easier to read, too. You only need to take a glimpse at the html version of this page to see what I mean:</p>
<pre>
This line:
you have to use special escape codes, &quot;&amp;lt;&quot;, &quot;&amp;gt;&quot;, and &quot;&amp;amp;&quot;.

When escaped, it becomes:
you have to use special escape codes, &amp;quot;&amp;amp;lt;&amp;quot;, &amp;quot;&amp;amp;gt;&amp;quot;, and &amp;quot;&amp;amp;amp;&amp;quot;.

When escaped again, it becomes:
you have to use special escape codes, &amp;amp;quot;&amp;amp;amp;lt;&amp;amp;quot;, &amp;amp;quot;&amp;amp;amp;gt;&amp;amp;quot;, and &amp;amp;quot;&amp;amp;amp;amp;&amp;amp;quot;.
</pre>
<p>With the delim system, it would look like this:</p>
<pre>
This line:
you have to use special escape codes, &quot;&amp;lt;&quot;, &quot;&amp;gt;&quot;, and &quot;&amp;amp;&quot;.

When escaped, it becomes:
&#40;you have to use special escape codes, &quot;&amp;lt;&quot;, &quot;&amp;gt;&quot;, and &quot;&amp;amp;&quot;.&#41;

When escaped again, it becomes:
&#40;&#40;you have to use special escape codes, &quot;&amp;lt;&quot;, &quot;&amp;gt;&quot;, and &quot;&amp;amp;&quot;.&#41;&#41;
</pre>
<p>Or, for a fairer comparison:</p>
<pre>
This line:
* Outside of raw delims, backticks &quot;`&quot; are only allowed to start and end a raw delim.

When escaped, it becomes:
`&#40;* Outside of raw delims, backticks &quot;`&quot; are only allowed to start and end a raw delim.&#41;`

When escaped again, it becomes:
`a&#40;`&#40;* Outside of raw delims, backticks &quot;`&quot; are only allowed to start and end a raw delim.&#41;`&#41;a`
</pre>
<p>Even in the fairer comparison, the delim system gives a result which is much easier to read!</p>
<h2 id="Update_2021-02-16:_Raw_Line_Delims">Update 2021-02-16: Raw Line Delims</h2>
<p>Since line comments will probably be a common feature, and #`&#91;&#93;` is an ugly syntax for comments containing unmatched delims, I&#39;m adding these additional rules to the delim system:</p>
<ul>
  <li>A raw line delim is written `# and goes until the next carriage return &#40;\r&#41; or line feed &#40;\n&#41;.</li>
  <li>A raw line delim does not contain its closing newline character. It ends just before the newline.</li>
  <li>Any character is allowed in a raw line delim in any combination, except for carriage return and line feed.</li>
  <li>A raw line delim cannot have a pattern.</li>
</ul>
<p>Relatedly, I&#39;ve added a <a href="../wiki/delim-system.html" title="Delim System">wiki entry</a> for the delim system.</p>
<h1 id="Footer">Footer</h1>
<p>Date: 2021-02-15</p>
<p>Latest Edit: 2021-02-16</p>
<p>Author: Galene</p>
<p><img src="../../resources/images/GaleneAvatar_small.png" alt="Galene&#39;s Avatar &#40;small&#41;"></p>
<p>
  <a href="home.html">Galene&#39;s Personal Home Page</a><br>
  <a href="../../home.html">ARC Home Page</a>
</p>
</body>

</html>
]]></content:encoded>
  </item>
  <item>
    <title>Gemini</title>
    <link>https://rightsofcharacters.gitlab.io/personal/galene/2021-01-28_gemini.html</link>
    <description>Galene blog post</description>
    <pubDate>Thu, 28 Jan 2021 12:00:00 GMT</pubDate>
    <guid>https://rightsofcharacters.gitlab.io/personal/galene/2021-01-28_gemini.html</guid>
    <content:encoded><![CDATA[<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="../../resources/images/GaleneAvatar_small.png">
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://rightsofcharacters.gitlab.io/personal/galene/home.rss">
<title>Gemini -- 2021-01-28</title>
<style>
body {
 max-width: 650px; margin: 40px auto; padding: 0 10px;
 font: 18px/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
   "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
   "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
 color: #222; background: #eee;
}
h1, h2, h3 { line-height: 1.2; }
h1 { border-bottom: 2px solid #000; }
h2 { border-bottom: 1px solid #000; }
h3 { border-bottom: 1px solid #999; }
hr { border: 1px solid #000; }
hr.small { border-color: #999; }
ul { padding-left: 24px; }
pre, code.incode { white-space: pre-wrap; border: 1px solid #000; background: #fff }
a.wikilink { text-decoration: none; }
a.wikilink:hover { text-decoration: underline; }
.note { font-size: 10px; vertical-align: top; }
.cref:target { background: #ffa; }
p.cite:target { border: 2px solid #000; }
#toc_container {
 display: table; padding: 0 24px 0 8px;
 background: #ddd; border: 1px solid #777;
}
#toc_container ul { list-style-type: disc; list-style-position: inside; }
@media (prefers-color-scheme: dark) {
 body { color: #eee; background: #444; }
 a:link { color: #7bf; }
 a:visited { color: #ccf; }
 h1 { border-bottom-color: #ccc; }
 h2 { border-bottom-color: #ccc; }
 h3 { border-bottom-color: #666; }
 hr { border-color: #ccc; }
 hr.small { border-color: #666; }
 pre { background: #555; }
 #toc_container { background: #555; border-color: #ccc; }
}
</style>
</head>

<body>
<h3 id="Galene&#39;s_Blog_--_Advocates_for_the_Rights_of_Characters_&#40;ARC&#41;">Galene&#39;s Blog -- Advocates for the Rights of Characters &#40;ARC&#41;</h3>
<div id="toc_container">
<ul class="toc_list">
<b>Contents</b>
  <li><a href="#Gemini_--_2021-01-28">Gemini -- 2021-01-28</a></li>
  <ul class="toc_list">
    <li><a href="#About_Gemini">About Gemini</a></li>
    <li><a href="#Inner_Links">Inner Links</a></li>
    <li><a href="#Other_Extensions">Other Extensions</a></li>
    <ul class="toc_list">
      <li><a href="#Inline_Hints">Inline Hints</a></li>
      <li><a href="#Wikilinks">Wikilinks</a></li>
      <li><a href="#Links_to_Sections">Links to Sections</a></li>
      <li><a href="#Directives">Directives</a></li>
    </ul>
    <li><a href="#Having_Fun_:&#41;">Having Fun :&#41;</a></li>
  </ul>
  <li><a href="#Footer">Footer</a></li>
</ul>
</div>
<h1 id="Gemini_--_2021-01-28">Gemini -- 2021-01-28</h1>
<h2 id="About_Gemini">About Gemini</h2>
<p>When I first started working on <a href="../../home.html" title="ARC Home Page">this website</a>, I thought we might host it on <a href="https://gemini.circumlunar.space/" title="Project Gemini">Gemini</a><span class="note" title="External Link">&#91;^&#93;</span>. Gemini is a minimal internet protocol as well as a text-based format for pages.</p>
<p>The format for gemini pages is really simple. Plain text shows up as plain text. Otherwise you have headers, lists, quotes, block quotes, and links. Here are examples of how they look in gemini:</p>
<pre>
# Header
## Subheader
### Nested subheader
=&gt; example.com Link To Example Website

* List element
* Second list element
* ...

&gt; This is a quote.

 ```
 This is a block quote.
 Note: normally there&#39;s no space before the ```.
 ```
</pre>
<p>Examining the first 3 characters is always enough to determine what kind of line it is, which makes parsing extremely easy. And it&#39;s all just normal plain text.</p>
<p>The simplicity is nice. Even though we never ended up putting the website on gemini, even now every page is generated from gemini using a home-grown gemini to html converter, with just a bit of css to make the pages look nice and clean.</p>
<h2 id="Inner_Links">Inner Links</h2>
<p>An interesting aspect of gemini is that every link must be on its own line. This results in a &quot;clean, list-like organisation of content&quot;, according to the <a href="https://gemini.circumlunar.space/docs/faq.html" title="Project Gemini FAQ">Gemini FAQ</a><span class="note" title="External Link">&#91;^&#93;</span>.</p>
<p>While I agree that the list-like organization can be nice, I often had to end a paragraphs with &quot;here is a link to X:&quot;, which breaks the flow of reading, and adds a lot of unnecessary newlines. So I wanted to find a way to add inner links which didn&#39;t veer too far from the spirit of gemini.</p>
<p>And today I found the solution! Taking the first paragraph of this post as example, the gemini looks like this:</p>
<pre>
When I first started working on &lt;&lt;this website&gt;&gt;, I thought we might host it on &lt;&lt;Gemini&gt;&gt;. Gemini is a minimal internet protocol as well as a text-based format for pages.

=&gt; ../../home.gmi ARC Home Page
=&gt; https://gemini.circumlunar.space/ Project Gemini
</pre>
<p>Which becomes:</p>
<hr class="small">
<p>When I first started working on <a href="../../home.html" title="ARC Home Page">this website</a>, I thought we might host it on <a href="https://gemini.circumlunar.space/" title="Project Gemini">Gemini</a><span class="note" title="External Link">&#91;^&#93;</span>. Gemini is a minimal internet protocol as well as a text-based format for pages.</p>
<hr class="small">
<p>In places where I had two short paragraphs, each one ending with &quot;here is a link to X:&quot;, I can now combine them into one paragraph using inner links. This saves space and is more readable even in the plain text, so even the gemini benefits from this.</p>
<p>So I&#39;m very pleased with this solution! The plain text looks clean &#40;and therefore so does the gemini&#41;, the list-like organization is preserved, and the html gets to have a few inner links :&#41;</p>
<h2 id="Other_Extensions">Other Extensions</h2>
<h3 id="Inline_Hints">Inline Hints</h3>
<p>We&#39;ve added many other extensions to the gemini format in our html generator. One of these is a new &quot;inline hint&quot; link type, which looks like this:</p>
<pre>
==&gt; resources/images/ARCLogo_small.png ARC Logo &#40;small&#41;
</pre>
<p>This is for links which can be inlined, which is especially useful for images. In HTML the image is loaded automatically, but on gemini I imagine the user would have to choose whether or not to do any inlining, in order to preserve gemini&#39;s one request per page policy. For gemini clients who don&#39;t support inlining, this would behave just like a normal link.</p>
<p>This extension is unfortunately incompatible with existing gemini clients, who will not interpret this as a link.</p>
<h3 id="Wikilinks">Wikilinks</h3>
<p>Another extension is wikilinks. Surround some text with double square brackets, and the text will become a link to the corresponding webpage. Wikilinks look like this:</p>
<pre>
This is a wikilink: &#91;&#91;my link&#93;&#93;.
</pre>
<p>The &quot;my link&quot; text becomes a link to &quot;my-link.html&quot; &#40;or &quot;my-link.gmi&quot; on a gemini page&#41;.</p>
<p>This does somewhat break the &quot;list-like organisation&quot; of links, but I think it&#39;s fine, since the link stays on the same website, is easy to understand, and looks fine even on gemini viewers that don&#39;t support them.</p>
<h3 id="Links_to_Sections">Links to Sections</h3>
<p>Our website has links to sections of a document using URI Fragments. For example here is a <a href="#Links_to_Sections" title="Link to this section">link to this section</a>. I&#39;m not sure if any gemini viewers support such links, but I think it&#39;s important to have a standard for this, because on very long pages being able to link to a specific section is very useful.</p>
<p>My proposal: a link like <code class="incode">=&gt; #My_Header</code> should jump to the first heading whose text is either exactly <code class="incode">My Header</code> or <code class="incode">My_Header</code>.</p>
<h3 id="Directives">Directives</h3>
<p>We&#39;ve also added all sorts of directives, configured using lines that look like <code class="incode">@@ key value</code>. For example, <code class="incode">@@ lang en</code> is on every page, and adds the <code class="incode">lang=&quot;en&quot;</code> attribute in the html. Another is the <code class="incode">@@ favicon</code>, to set the favicon. Or <code class="incode">@@ toc</code>, which is used to generate a table of contents. We use these for automation and metadata.</p>
<h2 id="Having_Fun_:&#41;">Having Fun :&#41;</h2>
<p>At this point we&#39;ve added so much stuff that we&#39;d need a gemini generator for our gemini pages!</p>
<p>I&#39;m having a lot of fun with this :&#41;</p>
<hr>
<p>Update 2021-02-11: This post has been slightly updated to use the new <code class="incode">inner block</code> formatting which was implemented today.</p>
<h1 id="Footer">Footer</h1>
<p>Date: 2021-01-28</p>
<p>Latest Edit: 2021-02-11</p>
<p>Author: Galene</p>
<p><img src="../../resources/images/GaleneAvatar_small.png" alt="Galene&#39;s Avatar &#40;small&#41;"></p>
<p>
  <a href="home.html">Galene&#39;s Personal Home Page</a><br>
  <a href="../../home.html">ARC Home Page</a>
</p>
</body>

</html>
]]></content:encoded>
  </item>
  <item>
    <title>First Post</title>
    <link>https://rightsofcharacters.gitlab.io/personal/galene/2021-01-26_first_post.html</link>
    <description>Galene blog post</description>
    <pubDate>Tue, 26 Jan 2021 12:00:00 GMT</pubDate>
    <guid>https://rightsofcharacters.gitlab.io/personal/galene/2021-01-26_first_post.html</guid>
    <content:encoded><![CDATA[<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="../../resources/images/GaleneAvatar_small.png">
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://rightsofcharacters.gitlab.io/personal/galene/home.rss">
<title>First Post -- 2021-01-26</title>
<style>
body {
 max-width: 650px; margin: 40px auto; padding: 0 10px;
 font: 18px/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
   "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
   "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
 color: #222; background: #eee;
}
h1, h2, h3 { line-height: 1.2; }
h1 { border-bottom: 2px solid #000; }
h2 { border-bottom: 1px solid #000; }
h3 { border-bottom: 1px solid #999; }
hr { border: 1px solid #000; }
hr.small { border-color: #999; }
ul { padding-left: 24px; }
pre, code.incode { white-space: pre-wrap; border: 1px solid #000; background: #fff }
a.wikilink { text-decoration: none; }
a.wikilink:hover { text-decoration: underline; }
.note { font-size: 10px; vertical-align: top; }
.cref:target { background: #ffa; }
p.cite:target { border: 2px solid #000; }
#toc_container {
 display: table; padding: 0 24px 0 8px;
 background: #ddd; border: 1px solid #777;
}
#toc_container ul { list-style-type: disc; list-style-position: inside; }
@media (prefers-color-scheme: dark) {
 body { color: #eee; background: #444; }
 a:link { color: #7bf; }
 a:visited { color: #ccf; }
 h1 { border-bottom-color: #ccc; }
 h2 { border-bottom-color: #ccc; }
 h3 { border-bottom-color: #666; }
 hr { border-color: #ccc; }
 hr.small { border-color: #666; }
 pre { background: #555; }
 #toc_container { background: #555; border-color: #ccc; }
}
</style>
</head>

<body>
<h3 id="Galene&#39;s_Blog_--_Advocates_for_the_Rights_of_Characters_&#40;ARC&#41;">Galene&#39;s Blog -- Advocates for the Rights of Characters &#40;ARC&#41;</h3>
<h1 id="First_Post_--_2021-01-26">First Post -- 2021-01-26</h1>
<h2 id="New_Blog,_New_Wiki">New Blog, New Wiki</h2>
<p>The blogging system seems to be working, so this is my first post! Though it probably won&#39;t show up on the actual website for a few days, or maybe even a few weeks, since there is incomplete work that needs to be finished before I&#39;ll upload everything.</p>
<p>Along with my personal blog, Emys and I also now have a <a href="../wiki/index.html" title="Personal Wiki Index">personal wiki</a>. I got the idea of making a wiki from the Agora:</p>
<p><a href="https://anagora.org/node/index">Agora Index</a><span class="note" title="External Link">&#91;^&#93;</span></p>
<p>I might update our home-grown static website generator to be able to export Agora-compatible Markdown. I&#39;m not sure I&#39;ll try to have it added to anagora.org though, since I wouldn&#39;t want to hinder Agora&#39;s growth by associating it with ARC &#40;I expect ARC will be unpopular with a lot of potential Agora users&#41;.</p>
<h2 id="Sources">Sources</h2>
<p>I&#39;m going to aim for us to have citations and sources in the personal wiki. I don&#39;t have much experience with citations, but it seems important to have norms about these things, for the health of distributed knowledge systems.</p>
<h2 id="Fediverse">Fediverse</h2>
<p>My Mastodon account is going pretty well! There are two things helping me be less anxious than I would normally be. First is that I&#39;m being very upfront about my belief that characters are people; anyone who considers following me will see it clearly stated on my profile. Second is that I joined a relatively big and generalist instance, so I&#39;m not worried about annoying people on the local timeline.</p>
<p>I even got one follower somehow, who is very nice!</p>
<h2 id="CSS">CSS</h2>
<p>The website&#39;s CSS keeps growing; there are a few more lines now. Currently the CSS is embedded directly into each page, and some parts of it only appear when necessary &#40;such as the table of contents CSS&#41;, but I should probably consider putting it into its own file.</p>
<p>The pro of having the CSS in its own file is that it gets cached. The con is that it increases the number of requests to load a page. Currently each page makes only one extra request to load favicon/logo. Minimizing the number of requests seems desirable.</p>
<p>Since the CSS is only about 1 kilobyte at this point, and becomes around 400 bytes when compressed with the webpage, I&#39;m definitely overthinking this. It&#39;s definitely fine to embed it in every page. Still, hopefully this growth will settle down soon.</p>
<h2 id="End">End</h2>
<p>That&#39;s all for this post :&#41;</p>
<h1 id="Footer">Footer</h1>
<p>Date: 2021-01-26</p>
<p>Author: Galene</p>
<p><img src="../../resources/images/GaleneAvatar_small.png" alt="Galene&#39;s Avatar &#40;small&#41;"></p>
<p>
  <a href="home.html">Galene&#39;s Personal Home Page</a><br>
  <a href="../../home.html">ARC Home Page</a>
</p>
</body>

</html>
]]></content:encoded>
  </item>
</channel>
</rss>
