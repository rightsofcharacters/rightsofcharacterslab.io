<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="../../resources/images/GaleneEmysWiki_small.png">
<title>Delim System</title>
<style>
body {
 max-width: 650px; margin: 40px auto; padding: 0 10px;
 font: 18px/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
   "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
   "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
 color: #222; background: #eee;
}
h1, h2, h3 { line-height: 1.2; }
h1 { border-bottom: 2px solid #000; }
h2 { border-bottom: 1px solid #000; }
h3 { border-bottom: 1px solid #999; }
hr { border: 1px solid #000; }
hr.small { border-color: #999; }
ul { padding-left: 24px; }
pre, code.incode { white-space: pre-wrap; border: 1px solid #000; background: #fff }
a.wikilink { text-decoration: none; }
a.wikilink:hover { text-decoration: underline; }
.note { font-size: 10px; vertical-align: top; }
.cref:target { background: #ffa; }
p.cite:target { border: 2px solid #000; }
#toc_container {
 display: table; padding: 0 24px 0 8px;
 background: #ddd; border: 1px solid #777;
}
#toc_container ul { list-style-type: disc; list-style-position: inside; }
@media (prefers-color-scheme: dark) {
 body { color: #eee; background: #444; }
 a:link { color: #7bf; }
 a:visited { color: #ccf; }
 h1 { border-bottom-color: #ccc; }
 h2 { border-bottom-color: #ccc; }
 h3 { border-bottom-color: #666; }
 hr { border-color: #ccc; }
 hr.small { border-color: #666; }
 pre { background: #555; }
 #toc_container { background: #555; border-color: #ccc; }
}
</style>
</head>

<body>
<h3 id="Personal_Wiki_of_Galene_and_Emys">Personal Wiki of Galene and Emys</h3>
<div id="toc_container">
<ul class="toc_list">
<b>Contents</b>
  <li><a href="#Delim_System">Delim System</a></li>
  <ul class="toc_list">
    <li><a href="#Rules">Rules</a></li>
    <li><a href="#Comments">Comments</a></li>
    <li><a href="#Raw_Line_Delims">Raw Line Delims</a></li>
    <li><a href="#Escape_Codes">Escape Codes</a></li>
    <li><a href="#Citations">Citations</a></li>
  </ul>
  <li><a href="#Footer">Footer</a></li>
</ul>
</div>
<h1 id="Delim_System">Delim System</h1>
<p>The delim system is a small collection of rules in the design of programming and data languages. Any language that follows these rules can be embedded in any other language that follows these rules.</p>
<p>In the delim system, there are 3 pairs of delims: &#40;&#41;, &#91;&#93;, and {}. There are also raw delims, which start with a backtick, then have a sequence of characters called the <i>pattern</i>, and finally an opening delim. They are then closed by the matching closing delim, followed by a repeat of the pattern, and finally another backtick.<span id="cref_1!_0" class="note cref"><a href="#cite_1!">[1!]</a></span></p>
<p>Valid delims:</p>
<pre>
&#40;A delim in parentheses&#41;
`&#40;A raw delim&#41;`
`hello&#40;A raw delim with &quot;hello&quot; as the pattern&#41;hello`
`x&#40; A `x&#40;&#41;` confusing &#41;y` raw &#93;x` delim ` that ends here-&gt;&#41;x`
</pre>
<p>Invalid delims:</p>
<pre>
&#40;This delim doesn&#39;t match with the correct one&#93;
&#40;This delim contains a ` backtick&#41;
`!&#40;This delim contains an illegal character in the pattern&#41;!`
</pre>
<h2 id="Rules">Rules<span id="cref_1!_1" class="note cref"><a href="#cite_1!">[1!]</a></span></h2>
<ul>
  <li>Inside a raw delim, any characters are allowed in any combination.</li>
  <li>There is one exception in a raw delim: it cannot contain its own closing pattern &#40;for example, a raw delim started by &quot;`a&#40;&quot; cannot contain &quot;&#41;a`&quot;&#41;.</li>
  <li>The pattern of a raw delim may only contain the characters &#91;a-zA-Z0-9_&#93;.</li>
  <li>Outside of raw delims, every parenthesis &#40;&#41;, square bracket &#91;&#93;, and curly brace {} must be properly matched.</li>
  <li>Outside of raw delims, backticks ` are only allowed to start and end a raw delim.</li>
  <li>There are no escape codes.</li>
</ul>
<p>There is also an additional rule for raw line delims, explained below.</p>
<h2 id="Comments">Comments<span id="cref_1!_2" class="note cref"><a href="#cite_1!">[1!]</a></span></h2>
<p>To preserve this system, comments that go to the end of line stop when they encounter a closing delim. And if they contain an open delim, they keep going until the end of the line after that delim is closed.</p>
<pre>
# This is a comment. &#40;It ends not here, -&gt;

  and not here-&gt;&#41;, but here -&gt;

print&#40;&quot;Hello, world!&quot; # This is a comment, and it ends here-&gt;&#41;;
</pre>
<p>Note that this is just a suggestion for how to support line comments in a delim language. Line comments like this are <i>not</i> part of the delim system. They can never cause delim errors, so any delim language can safely treat &quot;#&quot; like any other character.</p>
<p>Also note that line comments must not be removed, since that would make them be a sort of escape character. Instead they should be skipped over by the interpreter or compiler. For example if you have the following code, the text after the # should clearly not be removed:</p>
<pre>
html&#40;&lt;a href=&quot;#toc&quot;&gt;Jump To Table Of Contents&lt;/a&gt;&#41;
</pre>
<h2 id="Raw_Line_Delims">Raw Line Delims<span id="cref_1!_3" class="note cref"><a href="#cite_1!">[1!]</a></span></h2>
<p>Even though line comments are not part of the delim system, the fact that delim systems that support line comments cannot contain unmatched delims can be less pleasant. For this reason, we add these extra rules for raw line delims:</p>
<ul>
  <li>A raw line delim is written `# and goes until the next carriage return &#40;\r&#41; or line feed &#40;\n&#41;.</li>
  <li>A raw line delim does not contain its closing newline character. It ends just before the newline.</li>
  <li>Any character is allowed in a raw line delim in any combination, except for carriage return and line feed.</li>
  <li>A raw line delim cannot have a pattern.</li>
</ul>
<h2 id="Escape_Codes">Escape Codes<span id="cref_1!_4" class="note cref"><a href="#cite_1!">[1!]</a></span></h2>
<p>The rule is this: given a &quot;delim language&quot; following the delim rules and an &quot;escape language&quot; that has escape codes, the delim language can contain the escape language, but the escape language cannot contain the delim language.</p>
<p>For two simple examples where escape codes cause problems, if you tried to embed a delim language in html:</p>
<pre>
html&#40;&lt;delimLang&gt;&amp;#40; &#91;1, 2, 3&#93;&lt;/delimLang&gt;&#41;

html&#40;&lt;p&gt;&lt;/p&gt;&#41;
html&#40;&lt;delimLang&gt;html&#40;&amp;lt;p&amp;gt;&amp;lt;/p&amp;gt;&#41;&lt;/delimLang&gt;&#41;
</pre>
<p>In the first example, &amp;#40; is the escape code for an open parenthesis. This means the content of the html does not actually follow the delim rules, even though someone who didn&#39;t know html would think it did.</p>
<p>In the second example, we can see that due to escape codes, html cannot be embedded into itself without escaping, which breaks the delim system.</p>
<h2 id="Citations"><a class="wikilink" href="citation.html">Citations</a></h2>
<p>This idea is original to us. See the original blog post on the subject:</p>
<p id="cite_1!" class="cite">[1!] <span class="note">^</span> <a class="note" href="#cref_1!_0">0</a> <a class="note" href="#cref_1!_1">1</a> <a class="note" href="#cref_1!_2">2</a> <a class="note" href="#cref_1!_3">3</a> <a class="note" href="#cref_1!_4">4</a> <a href="https://rightsofcharacters.gitlab.io/personal/galene/2021-02-15_delims.html">Galene&#39;s Personal Blog. Delim System. 2021. Retrieved 2021-02-15.</a><span class="note" title="External Link">&#91;^&#93;</span></p>
<h1 id="Footer">Footer</h1>
<p>Date: 2021-02-15</p>
<p>Latest Edit: 2021-02-16</p>
<p>Author: <a class="wikilink" href="galene.html">Galene</a></p>
<p><img src="../../resources/images/GaleneEmysWiki_small.png" alt="Galene and Emys Wiki Logo &#40;small&#41;"></p>
<p><a href="index.html">Index</a></p>
<p><a href="../../home.html">ARC Home Page</a></p>
</body>

</html>
